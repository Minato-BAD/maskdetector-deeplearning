import numpy as np
import cv2
import os

import torch
import torchvision
from torchvision import transforms

import os
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import cv2
import numpy as np
import torch.nn as nn
import torch.nn.functional as F



from PIL import Image

#Resolving issues on MAC OS
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

classes = ['No-Mask', 'Mask']

#Definition of the model to be able to load it
class MaskDetection(nn.Module):
    def __init__(self,n_classes=2):
        super(MaskDetection,self).__init__()
        self.conv1=nn.Conv2d(3, 100, kernel_size=3, padding=1)
        self.conv2=nn.Conv2d(100, 128, kernel_size=3, stride=1, padding=1)
            
        self.pool=nn.MaxPool2d(2, 2)
            
        self.conv3=nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1)
        self.conv4=nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1) 
            
        self.fc1=nn.Sequential(nn.Flatten(),nn.Linear(160000, 512))
        self.fc2=nn.Linear(512, 256)
        self.fc3=nn.Linear(256, n_classes)
        
    def forward(self, x):
        x = self.pool(F.relu(self.conv2(F.relu(self.conv1(x))))) # output: 128 x 8 x 8
        x = self.pool(F.relu(self.conv4(F.relu(self.conv3(x))))) # output: 256 x 4 x 4
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        
        return x

#Path to models
base_dir = os.path.dirname(__file__)
prototxt_path = os.path.join(base_dir + 'model_data/deploy.prototxt')
faceDetect_path = os.path.join(base_dir + 'model_data/faceDetect.caffemodel')
maskDetect_path = os.path.join(base_dir + 'model_data/modelf.pth')

#Loading face detection model and mask detection model
print("------------ Loading models ------------")
maskDetect = torch.load(maskDetect_path,map_location=torch.device('cpu'))
maskDetect.eval()
faceDetect = cv2.dnn.readNetFromCaffe(prototxt_path, faceDetect_path)
device = torch.device("cpu")
maskDetect.to(device)

#Function to detect face
def detect_face(image):
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
    faceDetect.setInput(blob)
    detections = faceDetect.forward()
    faces=[]
    positions=[]
    for i in range(0, detections.shape[2]):
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        (startX,startY)=(max(0,startX-15),max(0,startY-15))
        (endX,endY)=(min(w-1,endX+15),min(h-1,endY+15))
        confidence = detections[0, 0, i, 2]
        # If confidence > 0.5, show box around face
        if (confidence > 0.5):
            face = frame[startY:endY, startX:endX]
            faces.append(face)
            positions.append((startX,startY,endX,endY))
    return faces,positions

#Function to detect mask
def detect_mask(faces):
    predictions = []
    image_transforms = transforms.Compose([transforms.Resize(size=(100,100)), transforms.ToTensor()])
    if (len(faces)>0):
        for img in faces:
            img = Image.fromarray(img)
            img = image_transforms(img)
           # img = img.permute((2, 0,1)) # model expects image to be of shape [3, 100, 100]
            img = img.unsqueeze(dim=0).float() 
            img = img.to(device) 
            pred = maskDetect(img)
            _, preds = torch.max(pred, dim=1)
            predictions.append(classes[preds.item()])         
    return predictions
    
#Video streaming
cap = cv2.VideoCapture(0)
while(True):
    ret, frame = cap.read()
    
    (faces,postions) = detect_face(frame)
    predictions=detect_mask(faces)
    
    for(box,prediction) in zip(postions,predictions):
        (startX, startY, endX, endY) = box
        label = "Mask" if prediction == "Mask" else "No Mask"
        color = (0, 255, 0) if label == "Mask" else (0,0,255)
        cv2.putText(frame, label, (startX, startY - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
        cv2.rectangle(frame,(startX, startY),(endX, endY),color,2)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
