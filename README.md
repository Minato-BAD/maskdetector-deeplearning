# Face Mask Detector Project
 
 This project is a simple Face Mask Detection project built with PyTorch and OpenCV using Deep Learning and Computer Vision concepts in order to detect face masks in images and in webcam video streams.

 # Data and training

:small_blue_diamond: The data used for this project is available [here](https://drive.google.com/drive/folders/1wOR7zxtUgvylGEatkWGSV7444-wA9fHt?usp=sharing).
 
:small_blue_diamond: In order to run the project the data needs to be downloaded from this link and added to project directory following this structure : 
 
```
.Train_Model
├── data                       
│   ├── train                  
│       ├── mask
│       └── no_mask
        └── npy 
│   └── test                   
│       ├── mask
│       └── no_mask 
└── MaskClassification.ipynb 
```
:small_blue_diamond: The train folder must contain the data downloaded. The test folder contains already some provided pictures to test but you may add any picture you want to test individuaally using the function defined in the notebook.

:small_blue_diamond: After creating the dataset the model can be trained and saved using 'MaskClassification.ipynb' on Train_Model folder.

# Video Stream

:small_blue_diamond: The model can be downloaded [here](https://drive.google.com/drive/folders/1wOR7zxtUgvylGEatkWGSV7444-wA9fHt?usp=sharing), following the path Video_Test\model_data\modelf.pth

```
.Use_Model
├── model_data 
    └── Make sure the model is here
└── VideoTest.py 
```

:small_blue_diamond: The trained model must be downloaded to run 'VideoTest.py' (on Use_Model folder) for face mask detection on webcam video stream. 

 # Team members
 Maria TERRAH
 Minato BEN AHMED DAHO
